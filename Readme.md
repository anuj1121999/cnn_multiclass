Convolution Neural Network for Multiclass classification
The CNN is used to predict the number represented by an image(1-5).
CNN structure:CONV2D -> RELU -> MAXPOOL -> CONV2D -> RELU -> MAXPOOL -> FLATTEN -> FULLYCONNECTED
Implemented using TensorFlow in Python.The classifier has a test accuracy of 83%.

